import 'package:atlan_sport/widgets/header.dart';
import 'package:atlan_sport/widgets/mycolumntable.dart';
import 'package:atlan_sport/widgets/mydatatable.dart';
import 'package:atlan_sport/widgets/mydropdown.dart';
import 'package:atlan_sport/widgets/mytextfield.dart';
import 'package:flutter/material.dart';
import 'package:atlan_sport/widgets/mytext.dart';
import 'package:atlan_sport/widgets/mytitlepage.dart';
import 'package:atlan_sport/services/mycolors.dart';

class Penjualan extends StatefulWidget {
  const Penjualan({Key? key}) : super(key: key);

  @override
  _PenjualanState createState() => _PenjualanState();
}

class _PenjualanState extends State<Penjualan> {
  @override
  Widget build(BuildContext context) {
    final List<String> _locations = [
      'Kode Stock',
      'Nama Barang',
      'Lokasi',
      'Size',
      'Warna',
      'Tanggal'
    ];
    String _txtLocation = "";

    return Scaffold(
      body: SafeArea(
        child: Container(
          color: MyColors.whiteColor,
          child: ListView(
            padding: EdgeInsets.symmetric(horizontal: 80),
            children: [
              Header(),
              Center(child: MyTitlepage(text: 'PENJUALAN')),
              SizedBox(height: 67),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  MyText(
                      text: "Search by",
                      fontSize: 20,
                      fontColor: MyColors.hitam,
                      fontWeight: FontWeight.w700),
                  SizedBox(width: 20),
                  SizedBox(
                    width: 180,
                    child: MyDropdown(
                        showTitle: false,
                        value: _txtLocation,
                        items: _locations.map((value) {
                          return DropdownMenuItem(
                            child: Text(value),
                            value: value,
                          );
                        }).toList(),
                        onChanged: (val) {
                          setState(() {
                            _txtLocation = val;
                          });
                        }),
                  ),
                  SizedBox(width: 20),
                  SizedBox(width: 264, child: MyTextFieldSearch()),
                ],
              ),
              SizedBox(height: 40),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 22, vertical: 30),
                color: MyColors.bgTable,
                child: Column(
                  children: [
                    Row(
                      children: [
                        MyColumnTable(label: "No"),
                        SizedBox(width: 1),
                        MyColumnTable(label: "Tanggal"),
                        SizedBox(width: 1),
                        MyColumnTable(label: "Kode Stock"),
                        SizedBox(width: 1),
                        Expanded(child: MyColumnTable(label: "Nama Barang")),
                        SizedBox(width: 1),
                        MyColumnTable(label: "Size"),
                        SizedBox(width: 1),
                        MyColumnTable(label: "Warna"),
                        SizedBox(width: 1),
                        MyColumnTable(label: "Qty"),
                        SizedBox(width: 1),
                        Expanded(child: MyColumnTable(label: "Harga")),
                        SizedBox(width: 1),
                        MyColumnTable(label: "Disc"),
                        SizedBox(width: 1),
                        MyColumnTable(label: "Laba"),
                      ],
                    ),
                    SizedBox(height: 2),
                    ListView.builder(
                      shrinkWrap: true,
                      physics: NeverScrollableScrollPhysics(),
                      itemCount: 20,
                      itemBuilder: (context, index) => Column(
                        children: [
                          Row(
                            children: [
                              MyDataTable(text: "No"),
                              SizedBox(width: 1),
                              MyDataTable(text: "Tanggal"),
                              SizedBox(width: 1),
                              MyDataTable(text: "Kode Stock"),
                              SizedBox(width: 1),
                              Expanded(child: MyDataTable(text: "Nama Barang")),
                              SizedBox(width: 1),
                              MyDataTable(text: "Size"),
                              SizedBox(width: 1),
                              MyDataTable(text: "Warna"),
                              SizedBox(width: 1),
                              MyDataTable(text: "Qty"),
                              SizedBox(width: 1),
                              Expanded(child: MyDataTable(text: "Harga")),
                              SizedBox(width: 1),
                              MyDataTable(text: "Disc"),
                              SizedBox(width: 1),
                              MyDataTable(text: "Laba"),
                            ],
                          ),
                          SizedBox(height: 1),
                        ],
                      ),
                    )
                  ],
                ),
              ),
              SizedBox(height: 40),
              Container(
                padding: EdgeInsets.only(right: 100),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        MyText(
                          text: 'Total Penjualan',
                          fontColor: MyColors.hitam,
                          fontSize: 18,
                          fontWeight: FontWeight.w500,
                        ),
                        SizedBox(height: 27),
                        MyText(
                          text: 'Total Laba',
                          fontColor: MyColors.hitam,
                          fontSize: 18,
                          fontWeight: FontWeight.w500,
                        ),
                      ],
                    ),
                    SizedBox(width: 20),
                    Column(
                      children: [
                        SizedBox(width: 345, child: MyTextFieldAbu()),
                        SizedBox(height: 10),
                        SizedBox(width: 345, child: MyTextFieldAbu()),
                      ],
                    ),
                  ],
                ),
              ),
              SizedBox(height: 80),
            ],
          ),
        ),
      ),
    );
  }
}
