import 'package:atlan_sport/widgets/header.dart';
import 'package:atlan_sport/widgets/mybutton.dart';
import 'package:atlan_sport/widgets/mycolumntable.dart';
import 'package:atlan_sport/widgets/mydatatable.dart';
import 'package:flutter/material.dart';
import 'package:atlan_sport/widgets/mytext.dart';
import 'package:atlan_sport/widgets/mytitlepage.dart';
import 'package:atlan_sport/widgets/mytextfield.dart';
import 'package:atlan_sport/services/mycolors.dart';

class Pengeluaran extends StatefulWidget {
  const Pengeluaran({Key? key}) : super(key: key);

  @override
  _PengeluaranState createState() => _PengeluaranState();
}

class _PengeluaranState extends State<Pengeluaran> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          color: MyColors.whiteColor,
          child: ListView(
            padding: EdgeInsets.symmetric(horizontal: 80),
            children: [
              Header(),
              Center(child: MyTitlepage(text: 'PENGELUARAN')),
              SizedBox(height: 67),

              Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      width: 180,
                      child: MyTextField(
                        label: "No. Pengeluaran",
                      ),
                    ),
                    SizedBox(height: 20),
                    Row(
                      children: [
                        SizedBox(
                          width: 180,
                          child: MyTextField(
                            label: "Nama Barang",
                          ),
                        ),
                        SizedBox(width: 60),
                        SizedBox(
                            width: 180,
                            child: MyTextFieldDate(
                              label: "Tanggal",
                            )),
                        SizedBox(width: 60),
                        SizedBox(
                          width: 180,
                          child: MyTextField(
                            label: "Harga",
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: 20),
                    MyTextField(label: "Keperluan"),
                    SizedBox(height: 20),
                    MyTextField(label: "Keterangan"),
                    SizedBox(height: 20),
                    Align(
                        alignment: Alignment.bottomRight,
                        child: MyButton(onPressed: () {}, text: "INPUT"))
                  ],
                ),
              ),
              SizedBox(height: 40),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 22, vertical: 30),
                color: MyColors.bgTable,
                child: Column(
                  children: [
                    Row(
                      children: [
                        MyColumnTable(label: "No"),
                        SizedBox(width: 1),
                        MyColumnTable(label: "No. Pengeluaran"),
                        SizedBox(width: 1),
                        MyColumnTable(label: "Tanggal"),
                        SizedBox(width: 1),
                        Expanded(child: MyColumnTable(label: "Keperluan")),
                        SizedBox(width: 1),
                        MyColumnTable(label: "Harga"),
                        SizedBox(width: 1),
                        Expanded(child: MyColumnTable(label: "Keterangan")),
                      ],
                    ),
                    SizedBox(height: 2),
                    ListView.builder(
                      shrinkWrap: true,
                      physics: NeverScrollableScrollPhysics(),
                      itemCount: 5,
                      itemBuilder: (context, index) => Column(
                        children: [
                          Row(
                            children: [
                              MyDataTable(text: "No"),
                              SizedBox(width: 1),
                              MyDataTable(text: "No. Pengeluaran"),
                              SizedBox(width: 1),
                              MyDataTable(text: "Tanggal"),
                              SizedBox(width: 1),
                              Expanded(child: MyDataTable(text: "Keperluan")),
                              SizedBox(width: 1),
                              MyDataTable(text: "Harga"),
                              SizedBox(width: 1),
                              Expanded(child: MyDataTable(text: "Keterangan")),
                            ],
                          ),
                          SizedBox(height: 1),
                        ],
                      ),
                    )
                  ],
                ),
              ),
              SizedBox(height: 44),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  MyText(
                    text: 'Total Pengeluaran',
                    fontColor: MyColors.hitam,
                    fontSize: 18,
                    fontWeight: FontWeight.w500,
                  ),
                  SizedBox(width: 20),
                  SizedBox(width: 345, child: MyTextFieldAbu())
                ],
              ),
              SizedBox(height: 80),
            ],
          ),
        ),
      ),
    );
  }
}
