import 'package:atlan_sport/views/internal/penjualan/mutasi2.dart';
import 'package:atlan_sport/widgets/header.dart';
import 'package:atlan_sport/widgets/mybutton.dart';
import 'package:atlan_sport/widgets/mycolumntable.dart';
import 'package:atlan_sport/widgets/mydatatable.dart';
import 'package:atlan_sport/widgets/mytext.dart';
import 'package:atlan_sport/widgets/mytextfield.dart';
import 'package:atlan_sport/widgets/mytitlepage.dart';
import 'package:flutter/material.dart';
import 'package:atlan_sport/services/mycolors.dart';
import 'package:get/get.dart';

class Mutasi extends StatelessWidget {
  const Mutasi({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final TextEditingController _txtNama = TextEditingController(),
        _txtAlamat = TextEditingController(),
        _txtNomor = TextEditingController(),
        _txtKodeStock = TextEditingController(),
        _txtQty = TextEditingController();

    return Scaffold(
      body: SafeArea(
          child: ListView(
        padding: EdgeInsets.symmetric(horizontal: 80),
        children: [
          Header(),
          Center(child: MyTitlepage(text: 'SURAT JALAN MUTASI')),
          SizedBox(height: 60),
          MutasiCont1(
              txtNama: _txtNama, txtAlamat: _txtAlamat, txtNomor: _txtNomor),
          SizedBox(height: 40),
          MutasiTable(txtKodeStock: _txtKodeStock, txtQty: _txtQty),
          SizedBox(
            height: 50,
          )
        ],
      )),
    );
  }
}

class MutasiTable extends StatelessWidget {
  const MutasiTable({
    Key? key,
    required TextEditingController txtKodeStock,
    required TextEditingController txtQty,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(40),
      decoration: BoxDecoration(
          color: MyColors.whiteColor,
          borderRadius: BorderRadius.circular(10),
          boxShadow: [
            BoxShadow(
              blurRadius: 2,
              spreadRadius: 3,
              color: Colors.grey.withOpacity(0.5),
            ),
          ]),
      child: Column(
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Row(
                children: [
                  SizedBox(width: 180, child: MyTextField(label: 'Kode Stock')),
                  SizedBox(width: 65),
                  SizedBox(width: 180, child: MyTextField(label: 'Qty')),
                  SizedBox(width: 65),
                ],
              ),
              MyButton(onPressed: () {}, text: 'INPUT'),
            ],
          ),
          SizedBox(height: 50),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 22, vertical: 30),
            color: MyColors.bgTable,
            child: Column(
              children: [
                Row(
                  children: [
                    MyColumnTable(label: "No"),
                    SizedBox(width: 1),
                    MyColumnTable(label: "Kode Stock"),
                    SizedBox(width: 1),
                    Expanded(child: MyColumnTable(label: "Nama Barang")),
                    SizedBox(width: 1),
                    MyColumnTable(label: "Warna"),
                    SizedBox(width: 1),
                    MyColumnTable(label: "Size"),
                    SizedBox(width: 1),
                    MyColumnTable(label: "Qty"),
                    SizedBox(width: 1),
                    MyColumnTable(label: "Harga Satuan"),
                    SizedBox(width: 1),
                    MyColumnTable(label: "Discount"),
                    SizedBox(width: 1),
                    MyColumnTable(label: "Total"),
                  ],
                ),
                SizedBox(height: 2),
                ListView.builder(
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  itemCount: 5,
                  itemBuilder: (context, index) => Column(
                    children: [
                      Row(
                        children: [
                          MyDataTable(text: "No"),
                          SizedBox(width: 1),
                          MyDataTable(text: "Kode Stock"),
                          SizedBox(width: 1),
                          Expanded(child: MyDataTable(text: "Nama Barang")),
                          SizedBox(width: 1),
                          MyDataTable(text: "Warna"),
                          SizedBox(width: 1),
                          MyDataTable(text: "Size"),
                          SizedBox(width: 1),
                          MyDataTable(text: "Qty"),
                          SizedBox(width: 1),
                          MyDataTable(text: "Harga Satuan"),
                          SizedBox(width: 1),
                          MyDataTable(text: "Discount"),
                          SizedBox(width: 1),
                          MyDataTable(text: "Total"),
                        ],
                      ),
                      SizedBox(height: 1),
                    ],
                  ),
                )
              ],
            ),
          ),
          SizedBox(height: 24),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                children: [
                  MyText(text: 'Grand Total'),
                  SizedBox(width: 40),
                  Center(
                      child: SizedBox(
                          width: 362,
                          child: MyTextField(
                            showLabel: false,
                          )))
                ],
              ),
              MyButton(onPressed: () => Get.to(Mutasi2()), text: 'DONE')
            ],
          ),
        ],
      ),
    );
  }
}

class MutasiCont1 extends StatelessWidget {
  const MutasiCont1({
    Key? key,
    required TextEditingController txtNama,
    required TextEditingController txtAlamat,
    required TextEditingController txtNomor,
  })  : _txtNama = txtNama,
        _txtAlamat = txtAlamat,
        _txtNomor = txtNomor,
        super(key: key);

  final TextEditingController _txtNama;
  final TextEditingController _txtAlamat;
  final TextEditingController _txtNomor;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(40),
      decoration: BoxDecoration(
          color: MyColors.whiteColor,
          borderRadius: BorderRadius.circular(10),
          boxShadow: [
            BoxShadow(
              blurRadius: 2,
              spreadRadius: 3,
              color: Colors.grey.withOpacity(0.5),
            ),
          ]),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          MyTextField(
              controller: _txtNama, label: "Nama", hintText: 'Lorem Ipsum'),
          SizedBox(height: 20),
          MyTextField(
              controller: _txtAlamat,
              height: 100,
              label: "Alamat",
              hintText:
                  'Lorem ipsum dolor sit amet, consectetur adipiscing elit.'),
          SizedBox(height: 20),
          MyTextField(
              controller: _txtNomor,
              label: "No. Telepon",
              hintText: '08xx xxxx xxxx'),
        ],
      ),
    );
  }
}
