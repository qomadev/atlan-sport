import 'package:atlan_sport/widgets/header.dart';
import 'package:atlan_sport/widgets/mybutton.dart';
import 'package:atlan_sport/widgets/mycolumntable.dart';
import 'package:atlan_sport/widgets/mydatatable.dart';
import 'package:atlan_sport/widgets/mytext.dart';
import 'package:atlan_sport/widgets/mytextfield.dart';
import 'package:atlan_sport/widgets/mytitlepage.dart';
import 'package:flutter/material.dart';
import 'package:atlan_sport/services/mycolors.dart';

class DirectOrder2 extends StatefulWidget {
  const DirectOrder2({Key? key}) : super(key: key);

  @override
  _DirectOrder2State createState() => _DirectOrder2State();
}

class _DirectOrder2State extends State<DirectOrder2> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
          child: Container(
        color: MyColors.whiteColor,
        child: ListView(
          padding: EdgeInsets.symmetric(horizontal: 80),
          children: [
            Header(),
            Center(child: MyTitlepage(text: 'FAKTUR PENJUALAN')),
            SizedBox(height: 40),
            DirectOrderCont1(),
            SizedBox(height: 40),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                MyButton(onPressed: () {}, text: "CETAK"),
              ],
            ),
            SizedBox(height: 80),
          ],
        ),
      )),
    );
  }
}

class DirectOrderCont1 extends StatelessWidget {
  const DirectOrderCont1({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(40),
      decoration: BoxDecoration(
          color: MyColors.whiteColor,
          border: Border.all(color: MyColors.hitam)),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Image.asset('assets/images/do/logo_atlan.png', scale: 5),
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Center(
                child: MyText(
                  text: 'FAKTUR PENJUALAN',
                  fontColor: MyColors.hitam,
                  fontSize: 50,
                  fontWeight: FontWeight.w700,
                ),
              ),
              SizedBox(height: 2),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  MyText(
                    text: 'No. Invoice',
                    fontColor: MyColors.hitam,
                    fontWeight: FontWeight.w500,
                  ),
                  SizedBox(width: 40),
                  MyText(
                    text: '1234567890',
                    fontColor: MyColors.hitam,
                  )
                ],
              )
            ],
          ),
          SizedBox(height: 40),
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    MyText(
                      text: 'Nama',
                      fontColor: MyColors.hitam,
                      fontWeight: FontWeight.w500,
                    ),
                    SizedBox(height: 1),
                    MyText(
                      text: 'Lorem Ipsum',
                      fontColor: MyColors.hitam,
                      fontSize: 20,
                    ),
                    SizedBox(height: 16),
                    MyText(
                      text: 'Alamat',
                      fontColor: MyColors.hitam,
                      fontWeight: FontWeight.w500,
                    ),
                    SizedBox(height: 1),
                    MyText(
                      text:
                          'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
                      fontColor: MyColors.hitam,
                      fontSize: 20,
                    ),
                    SizedBox(height: 16),
                    MyText(
                      text: 'No. Telepon',
                      fontColor: MyColors.hitam,
                      fontWeight: FontWeight.w500,
                    ),
                    SizedBox(height: 1),
                    MyText(
                      text: '08xx xxxx xxxx',
                      fontColor: MyColors.hitam,
                      fontSize: 20,
                    ),
                  ],
                ),
              ),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    MyText(
                      text: 'Tanggal',
                      fontColor: MyColors.hitam,
                      fontWeight: FontWeight.w500,
                    ),
                    SizedBox(height: 1),
                    MyText(
                      text: 'xx / Xxxx / XXXX',
                      fontColor: MyColors.hitam,
                      fontSize: 20,
                    ),
                    SizedBox(height: 16),
                    MyText(
                      text: 'Nama Sells',
                      fontColor: MyColors.hitam,
                      fontWeight: FontWeight.w500,
                    ),
                    SizedBox(height: 1),
                    MyText(
                      text: 'Lorem ipsum',
                      fontColor: MyColors.hitam,
                      fontSize: 20,
                    ),
                  ],
                ),
              ),
            ],
          ),
          SizedBox(height: 30),
          Container(
            padding: EdgeInsets.all(40),
            decoration: BoxDecoration(
                color: MyColors.whiteColor,
                borderRadius: BorderRadius.circular(5),
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.5),
                  ),
                ]),
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 22, vertical: 30),
              color: MyColors.bgTable,
              child: Column(
                children: [
                  Row(
                    children: [
                      MyColumnTable(label: "No"),
                      SizedBox(width: 1),
                      MyColumnTable(label: "Kode Stock"),
                      SizedBox(width: 1),
                      Expanded(child: MyColumnTable(label: "Nama Barang")),
                      SizedBox(width: 1),
                      MyColumnTable(label: "Warna"),
                      SizedBox(width: 1),
                      MyColumnTable(label: "Size"),
                      SizedBox(width: 1),
                      MyColumnTable(label: "Qty"),
                      SizedBox(width: 1),
                      MyColumnTable(label: "Harga Satuan"),
                      SizedBox(width: 1),
                      MyColumnTable(label: "Discount"),
                      SizedBox(width: 1),
                      MyColumnTable(label: "Total"),
                    ],
                  ),
                  SizedBox(height: 2),
                  ListView.builder(
                    shrinkWrap: true,
                    physics: NeverScrollableScrollPhysics(),
                    itemCount: 5,
                    itemBuilder: (context, index) => Column(
                      children: [
                        Row(
                          children: [
                            MyDataTable(text: "No"),
                            SizedBox(width: 1),
                            MyDataTable(text: "Kode Stock"),
                            SizedBox(width: 1),
                            Expanded(child: MyDataTable(text: "Nama Barang")),
                            SizedBox(width: 1),
                            MyDataTable(text: "Warna"),
                            SizedBox(width: 1),
                            MyDataTable(text: "Size"),
                            SizedBox(width: 1),
                            MyDataTable(text: "Qty"),
                            SizedBox(width: 1),
                            MyDataTable(text: "Harga Satuan"),
                            SizedBox(width: 1),
                            MyDataTable(text: "Discount"),
                            SizedBox(width: 1),
                            MyDataTable(text: "Total"),
                          ],
                        ),
                        SizedBox(height: 1),
                      ],
                    ),
                  ),
                  SizedBox(height: 25),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      MyText(
                        text: 'Grand Total',
                        fontColor: MyColors.hitam,
                        fontSize: 18,
                        fontWeight: FontWeight.w500,
                      ),
                      SizedBox(width: 30),
                      SizedBox(width: 344, child: MyTextFieldAbu()),
                    ],
                  ),
                ],
              ),
            ),
          ),
          SizedBox(height: 40),
          Row(
            children: [
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  MyText(
                    text: 'Kasir',
                    fontColor: MyColors.hitam,
                    fontSize: 20,
                    fontWeight: FontWeight.w700,
                  ),
                  SizedBox(height: 60),
                  MyText(
                    text: '(......................................)',
                    fontColor: MyColors.hitam,
                    fontSize: 20,
                    fontWeight: FontWeight.w700,
                  )
                ],
              ),
              SizedBox(width: 200),
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  MyText(
                    text: 'Customer',
                    fontColor: MyColors.hitam,
                    fontSize: 20,
                    fontWeight: FontWeight.w700,
                  ),
                  SizedBox(height: 60),
                  MyText(
                    text: '(......................................)',
                    fontColor: MyColors.hitam,
                    fontSize: 20,
                    fontWeight: FontWeight.w700,
                  )
                ],
              ),
            ],
          ),
        ],
      ),
    );
  }
}
