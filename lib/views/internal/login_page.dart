import 'package:atlan_sport/widgets/header.dart';
import 'package:atlan_sport/widgets/mytextfield.dart';
import 'package:flutter/material.dart';
import 'package:atlan_sport/widgets/mybutton.dart';
import 'package:atlan_sport/services/mycolors.dart';
import 'package:atlan_sport/services/myfonts.dart';
import 'package:atlan_sport/views/internal/intern_page.dart';
import 'package:get/get.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GestureDetector(
        onTap: () {
          FocusScope.of(context).unfocus();
        },
        child: Container(
          color: MyColors.whiteColor,
          child: Column(
            children: [
              Header(),
              SizedBox(height: 135),
              LoginBody(),
            ],
          ),
        ),
      ),
    );
  }
}

class LoginBody extends StatelessWidget {
  const LoginBody({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 348,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Login',
            textAlign: TextAlign.left,
            style: TextStyle(
                fontFamily: MyFonts.dmsans,
                fontSize: 40,
                fontWeight: FontWeight.w700,
                color: MyColors.biru),
          ),
          SizedBox(height: 20),
          MyTextFieldNavy(hintText: "Username"),
          SizedBox(height: 16),
          MyTextFieldNavy(hintText: "Password", obscure: true),
          SizedBox(height: 26),
          Align(
            alignment: Alignment.centerRight,
            child: MyButton(
              onPressed: () => Get.to(InternPage()),
              text: 'MASUK',
            ),
          )
        ],
      ),
    );
  }
}
