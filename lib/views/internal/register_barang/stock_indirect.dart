import 'package:atlan_sport/widgets/header.dart';
import 'package:atlan_sport/widgets/mycolumntable.dart';
import 'package:atlan_sport/widgets/mydatatable.dart';
import 'package:atlan_sport/widgets/mydropdown.dart';
import 'package:flutter/material.dart';
import 'package:atlan_sport/widgets/mybutton.dart';
import 'package:atlan_sport/widgets/mytext.dart';
import 'package:atlan_sport/widgets/mytextfield.dart';
import 'package:atlan_sport/widgets/mytitlepage.dart';
import 'package:atlan_sport/services/mycolors.dart';

class StockIndirect extends StatefulWidget {
  const StockIndirect({Key? key}) : super(key: key);

  @override
  _StockIndirectState createState() => _StockIndirectState();
}

class _StockIndirectState extends State<StockIndirect> {
  final List<String> _listSize = ["S", "M", "L", "XL", "XXL"];
  String _txtSize = "";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          color: MyColors.whiteColor,
          child: ListView(
            padding: EdgeInsets.symmetric(horizontal: 80),
            children: [
              Header(),
              Center(child: MyTitlepage(text: 'STOCK INDIRECT')),
              SizedBox(height: 40),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      SizedBox(
                          width: 180,
                          child: MyTextField(
                            label: "Kode Stock",
                          )),
                      SizedBox(width: 60),
                      MyButton(onPressed: () {}, text: "CARI")
                    ],
                  ),
                  SizedBox(height: 20),
                  Row(
                    children: [
                      SizedBox(
                          width: 180,
                          child: MyTextField(
                            label: "Nama Barang",
                          )),
                      SizedBox(width: 60),
                      SizedBox(
                          width: 180,
                          child: MyTextField(
                            label: "Warna",
                          )),
                      SizedBox(width: 60),
                      SizedBox(
                        width: 180,
                        child: MyDropdown(
                            label: "Size",
                            value: _txtSize,
                            items: _listSize.map((value) {
                              return DropdownMenuItem(
                                child: Text(value),
                                value: value,
                              );
                            }).toList(),
                            onChanged: (val) {
                              setState(() {
                                _txtSize = val;
                              });
                            }),
                      ),
                      SizedBox(width: 60),
                      SizedBox(
                          width: 180,
                          child: MyTextField(
                            label: "Lokasi",
                          )),
                      SizedBox(width: 60),
                      SizedBox(
                          width: 180,
                          child: MyTextField(
                            label: "Qty",
                          )),
                    ],
                  ),
                  SizedBox(height: 20),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      SizedBox(
                          width: 180,
                          child: MyTextField(
                            label: "HPP",
                          )),
                      SizedBox(width: 60),
                      SizedBox(
                          width: 180,
                          child: MyTextField(
                            label: "HET",
                          )),
                      SizedBox(width: 60),
                      SizedBox(
                          width: 180,
                          child: MyTextField(
                            label: "QTY",
                          )),
                      SizedBox(width: 60),
                      MyButton(onPressed: () {}, text: "INPUT")
                    ],
                  ),
                ],
              ),
              SizedBox(height: 50),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 22, vertical: 30),
                color: MyColors.bgTable,
                child: Column(
                  children: [
                    Row(
                      children: [
                        MyColumnTable(label: "No"),
                        SizedBox(width: 1),
                        MyColumnTable(label: "Kode Stock"),
                        SizedBox(width: 1),
                        Expanded(child: MyColumnTable(label: "Nama Barang")),
                        SizedBox(width: 1),
                        MyColumnTable(label: "Warna"),
                        SizedBox(width: 1),
                        MyColumnTable(label: "Size"),
                        SizedBox(width: 1),
                        MyColumnTable(label: "HPP"),
                        SizedBox(width: 1),
                        MyColumnTable(label: "HET"),
                        SizedBox(width: 1),
                        MyColumnTable(label: "Qty"),
                        SizedBox(width: 1),
                        MyColumnTable(label: "Lokasi"),
                        SizedBox(width: 1),
                        MyColumnTable(label: "Qty"),
                      ],
                    ),
                    SizedBox(height: 2),
                    ListView.builder(
                      shrinkWrap: true,
                      physics: NeverScrollableScrollPhysics(),
                      itemCount: 11,
                      itemBuilder: (context, index) => Column(
                        children: [
                          Row(
                            children: [
                              MyDataTable(text: "No"),
                              SizedBox(width: 1),
                              MyDataTable(text: "Kode Stock"),
                              SizedBox(width: 1),
                              Expanded(child: MyDataTable(text: "Nama Barang")),
                              SizedBox(width: 1),
                              MyDataTable(text: "Warna"),
                              SizedBox(width: 1),
                              MyDataTable(text: "Size"),
                              SizedBox(width: 1),
                              MyDataTable(text: "HPP"),
                              SizedBox(width: 1),
                              MyDataTable(text: "HET"),
                              SizedBox(width: 1),
                              MyDataTable(text: "Qty"),
                              SizedBox(width: 1),
                              MyDataTable(text: "Lokasi"),
                              SizedBox(width: 1),
                              MyDataTable(text: "Qty"),
                            ],
                          ),
                          SizedBox(height: 1),
                        ],
                      ),
                    )
                  ],
                ),
              ),
              SizedBox(height: 30),
              Padding(
                padding: const EdgeInsets.only(right: 61),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      children: [
                        MyText(text: "Grand Total HPP"),
                        SizedBox(width: 40),
                        SizedBox(
                            width: 362,
                            child: Center(child: MyTextField(showLabel: false)))
                      ],
                    ),
                    MyButton(onPressed: () {}, text: 'DONE')
                  ],
                ),
              ),
              SizedBox(
                height: 80,
              )
            ],
          ),
        ),
      ),
    );
  }
}
