import 'package:atlan_sport/widgets/drawer.dart';
import 'package:atlan_sport/widgets/header.dart';
import 'package:atlan_sport/widgets/mytext.dart';
import 'package:flutter/material.dart';
import 'package:atlan_sport/services/mycolors.dart';

class InternPage extends StatefulWidget {
  const InternPage({Key? key}) : super(key: key);

  @override
  _InternPageState createState() => _InternPageState();
}

class _InternPageState extends State<InternPage> {
  final GlobalKey<ScaffoldState> _scaffoldState =
      new GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldState,
        drawer: MyDrawer(),
        body: SafeArea(
          child: Container(
            color: MyColors.whiteColor,
            child: ListView(
              children: [
                Column(children: [
                  Stack(
                    children: [
                      Header(),
                      GestureDetector(
                        onTap: () {
                          _scaffoldState.currentState!.openDrawer();
                          FocusScope.of(context).unfocus();
                        },
                        child: Container(
                          width: 16,
                          height: 12,
                          margin: EdgeInsets.only(left: 50, top: 60),
                          child: Image.asset("assets/icons/intern/drawer.png"),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 20),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          MyText(
                            text: "NAMA KARYAWAN YANG MELAKUKAN LOGIN",
                            fontWeight: FontWeight.w700,
                            fontSize: 23,
                          ),
                          SizedBox(height: 10),
                          MyText(
                            text: "Selamat Datang di Atlan Sport Admin Page",
                            fontWeight: FontWeight.w400,
                            fontSize: 20,
                          ),
                        ],
                      ),
                      SizedBox(width: 78),
                      Image.asset(
                        'assets/images/intern/img_intern.png',
                        width: 295,
                        height: 436,
                      )
                    ],
                  ),
                ]),
              ],
            ),
          ),
        ));
  }
}
