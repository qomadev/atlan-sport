import 'package:atlan_sport/views/internal/login_page.dart';
import 'package:flutter/material.dart';
import 'package:atlan_sport/services/mycolors.dart';
import 'package:atlan_sport/services/myfonts.dart';
import 'package:get/get.dart';

class DashboardPage extends StatefulWidget {
  const DashboardPage({Key? key}) : super(key: key);

  @override
  _DashboardPageState createState() => _DashboardPageState();
}

class _DashboardPageState extends State<DashboardPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 26,
        leadingWidth: MediaQuery.of(context).size.width,
        backgroundColor: MyColors.whiteColor,
        shadowColor: Color.fromRGBO(299, 299, 299, 0.2),
        elevation: 3,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Text(
              'Help',
              style: TextStyle(
                  fontFamily: MyFonts.dmsans,
                  fontSize: 12,
                  color: MyColors.hitam),
            ),
            SizedBox(width: 55),
            GestureDetector(
              onTap: () {
                Get.to(LoginPage());
              },
              child: Text(
                'Internal',
                style: TextStyle(
                    fontFamily: MyFonts.dmsans,
                    fontSize: 12,
                    color: MyColors.hitam),
              ),
            ),
            SizedBox(width: 55),
            Text(
              'Sign In',
              style: TextStyle(
                  fontFamily: MyFonts.dmsans,
                  fontSize: 12,
                  color: MyColors.hitam),
            ),
            SizedBox(width: 70)
          ],
        ),
      ),
      body: SafeArea(
        child: ListView(
          children: [
            Column(
              children: [
                SizedBox(
                  height: 47.29,
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  padding: EdgeInsets.only(left: 70, right: 70),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        child: Image.asset(
                          'assets/images/dashboard/img_listbar.png',
                          height: 12.2,
                          width: 16,
                        ),
                      ),
                      SizedBox(width: 100),
                      Text(
                        'Explore',
                        style: TextStyle(
                            fontFamily: MyFonts.dmsans,
                            fontSize: 16,
                            color: MyColors.darkGrey),
                      ),
                      SizedBox(
                        width: 50,
                      ),
                      Text(
                        'Store',
                        style: TextStyle(
                            fontFamily: MyFonts.dmsans,
                            fontSize: 16,
                            color: MyColors.darkGrey),
                      ),
                      SizedBox(
                        width: 100,
                      ),
                      Expanded(
                        child: Image.asset(
                          'assets/images/dashboard/logo_atlan.png',
                          height: 53,
                          width: 231,
                        ),
                      ),
                      SizedBox(
                        width: 300,
                      ),
                      Image.asset(
                        'assets/images/login/img_instagram.png',
                        height: 30,
                        width: 30,
                      )
                    ],
                  ),
                ),
                SizedBox(height: 50),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      padding: EdgeInsets.only(left: 80, right: 80),
                      child: Image.asset(
                        'assets/images/dashboard/img_dash.png',
                        fit: BoxFit.contain,
                      ),
                    ),
                    SizedBox(height: 50),
                    Text(
                      'make your support',
                      style: TextStyle(
                          fontFamily: MyFonts.dmsans,
                          fontSize: 40,
                          color: MyColors.biru),
                    ),
                    Text(
                      'More COMPORTABLE',
                      style: TextStyle(
                          fontSize: 76,
                          fontFamily: MyFonts.dmsans,
                          color: MyColors.biru),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 240),
                      child: Container(
                        child: Row(
                          children: [
                            Image.asset(
                              'assets/images/dashboard/img_awal.png',
                              width: 429,
                              height: 369,
                            ),
                            SizedBox(width: 50),
                            Expanded(
                              child: Text(
                                't Etiam turpis tortor, sagittis ut cursus et, viverra et sapien. \nNulla volutpat a ipsum vitae dignissim. Vivamus dolor nisi, \nelementum quis lacinia nec, ornare sit amet diam. In at mollis \nelit, quis mattis augue. Maecenas elementum, metus in \nposuere semper, massa nisl tempus massa, at tempus quam \nex eu ex. Quisque in eleifend tortor, eget imperdiet tellus. \nAliquam posuere dapibus luctus. Sed accumsan ullamcorper \nneque, ac placerat purus lobortis luctus.',
                                style: TextStyle(
                                    fontFamily: MyFonts.dmsans,
                                    fontSize: 18,
                                    color: MyColors.biru),
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 50),
                Align(
                  alignment: Alignment.centerLeft,
                  child: Padding(
                    padding: const EdgeInsets.only(left: 80),
                    child: Text(
                      'Comfortable Men',
                      style: TextStyle(
                          fontFamily: MyFonts.dmsans,
                          fontSize: 31,
                          color: MyColors.biru),
                    ),
                  ),
                ),
                SizedBox(height: 30),
                Container(
                  width: MediaQuery.of(context).size.width,
                  child: Expanded(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Image.asset(
                              'assets/images/dashboard/img_1.png',
                              height: 518,
                              width: 410,
                            ),
                            Text(
                              'T- Shirt.',
                              style: TextStyle(
                                  fontFamily: MyFonts.dmsans,
                                  fontSize: 16,
                                  color: MyColors.merah),
                            ),
                            Text(
                              'Lorem ipsum dolor sit amet',
                              style: TextStyle(
                                  fontFamily: MyFonts.dmsans,
                                  fontSize: 16,
                                  color: MyColors.grey1Color),
                            ),
                            Text(
                              'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
                              style: TextStyle(
                                  fontFamily: MyFonts.dmsans,
                                  fontSize: 16,
                                  color: MyColors.grey1Color),
                            ),
                            Text(
                              'Rp. 999.999',
                              style: TextStyle(
                                  fontFamily: MyFonts.dmsans,
                                  fontSize: 16,
                                  color: MyColors.merah),
                            ),
                          ],
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Image.asset(
                              'assets/images/dashboard/img_2.png',
                              height: 518,
                              width: 410,
                            ),
                            Text(
                              'T- Shirt.',
                              style: TextStyle(
                                  fontFamily: MyFonts.dmsans,
                                  fontSize: 16,
                                  color: MyColors.merah),
                            ),
                            Text(
                              'Lorem ipsum dolor sit amet',
                              style: TextStyle(
                                  fontFamily: MyFonts.dmsans,
                                  fontSize: 16,
                                  color: MyColors.grey1Color),
                            ),
                            Text(
                              'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
                              style: TextStyle(
                                  fontFamily: MyFonts.dmsans,
                                  fontSize: 16,
                                  color: MyColors.grey1Color),
                            ),
                            Text(
                              'Rp. 999.999',
                              style: TextStyle(
                                  fontFamily: MyFonts.dmsans,
                                  fontSize: 16,
                                  color: MyColors.merah),
                            ),
                          ],
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Image.asset(
                              'assets/images/dashboard/img_3.png',
                              height: 518,
                              width: 410,
                            ),
                            Text(
                              'T- Shirt.',
                              style: TextStyle(
                                  fontFamily: MyFonts.dmsans,
                                  fontSize: 16,
                                  color: MyColors.merah),
                            ),
                            Text(
                              'Lorem ipsum dolor sit amet',
                              style: TextStyle(
                                  fontFamily: MyFonts.dmsans,
                                  fontSize: 16,
                                  color: MyColors.grey1Color),
                            ),
                            Text(
                              'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
                              style: TextStyle(
                                  fontFamily: MyFonts.dmsans,
                                  fontSize: 16,
                                  color: MyColors.grey1Color),
                            ),
                            Text(
                              'Rp. 999.999',
                              style: TextStyle(
                                  fontFamily: MyFonts.dmsans,
                                  fontSize: 16,
                                  color: MyColors.merah),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
                SizedBox(height: 149),
                Container(
                  height: 333,
                  width: MediaQuery.of(context).size.width,
                  decoration:
                      BoxDecoration(color: MyColors.whiteColor, boxShadow: [
                    BoxShadow(
                        color: Color.fromRGBO(299, 299, 299, 0.2),
                        offset: Offset(0, 3),
                        blurRadius: 4,
                        spreadRadius: 7)
                  ]),
                  child: Padding(
                    padding:
                        const EdgeInsets.only(left: 80, right: 80, top: 60),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Column(
                          children: [
                            Text(
                              'ATLAN SPORT INDONESIA',
                              style: TextStyle(
                                  fontFamily: MyFonts.dmsans,
                                  fontSize: 26,
                                  fontWeight: FontWeight.w700,
                                  color: MyColors.biru),
                            ),
                            SizedBox(height: 9),
                            Text(
                              'Bandung, West Java, Indonesia',
                              style: TextStyle(
                                  fontFamily: MyFonts.dmsans,
                                  fontSize: 22,
                                  color: MyColors.biru),
                            ),
                            SizedBox(height: 108),
                            Container(
                              height: 1,
                              width: 310,
                              color: MyColors.biru,
                            ),
                            SizedBox(height: 15),
                            Text(
                              '2021 Qoma. All Rights Reserved',
                              style: TextStyle(
                                  fontFamily: MyFonts.dmsans,
                                  fontSize: 14,
                                  color: MyColors.biru),
                            )
                          ],
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              'FIND STORE',
                              style: TextStyle(
                                  fontFamily: MyFonts.dmsans,
                                  fontSize: 18,
                                  fontWeight: FontWeight.w500,
                                  color: MyColors.merah),
                            ),
                            SizedBox(height: 20),
                            Text(
                              'BECOME A MEMBER',
                              style: TextStyle(
                                  fontFamily: MyFonts.dmsans,
                                  fontSize: 18,
                                  fontWeight: FontWeight.w500,
                                  color: MyColors.merah),
                            ),
                            SizedBox(height: 20),
                            Text(
                              'SIGN UP FOR EMAIL',
                              style: TextStyle(
                                  fontFamily: MyFonts.dmsans,
                                  fontSize: 18,
                                  fontWeight: FontWeight.w500,
                                  color: MyColors.merah),
                            ),
                            SizedBox(height: 20),
                            Text(
                              'STUDENT DISCOUNT',
                              style: TextStyle(
                                  fontFamily: MyFonts.dmsans,
                                  fontSize: 18,
                                  fontWeight: FontWeight.w500,
                                  color: MyColors.merah),
                            ),
                            SizedBox(height: 20),
                            Text(
                              'SEND US A FEDDBACK',
                              style: TextStyle(
                                  fontFamily: MyFonts.dmsans,
                                  fontSize: 18,
                                  fontWeight: FontWeight.w500,
                                  color: MyColors.merah),
                            ),
                            SizedBox(height: 20),
                          ],
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              'FIND STORE',
                              style: TextStyle(
                                  fontFamily: MyFonts.dmsans,
                                  fontSize: 18,
                                  fontWeight: FontWeight.w500,
                                  color: MyColors.merah),
                            ),
                            SizedBox(height: 15),
                            Text(
                              'Order Status',
                              style: TextStyle(
                                  fontFamily: MyFonts.dmsans,
                                  fontSize: 18,
                                  color: MyColors.merah),
                            ),
                            SizedBox(height: 15),
                            Text(
                              'Delivery',
                              style: TextStyle(
                                  fontFamily: MyFonts.dmsans,
                                  fontSize: 18,
                                  color: MyColors.merah),
                            ),
                            SizedBox(height: 15),
                            Text(
                              'Returns',
                              style: TextStyle(
                                  fontFamily: MyFonts.dmsans,
                                  fontSize: 18,
                                  color: MyColors.merah),
                            ),
                            SizedBox(height: 15),
                            Text(
                              'Payment Options',
                              style: TextStyle(
                                  fontFamily: MyFonts.dmsans,
                                  fontSize: 18,
                                  color: MyColors.merah),
                            ),
                            SizedBox(height: 15),
                            Text(
                              'Contact Us',
                              style: TextStyle(
                                  fontFamily: MyFonts.dmsans,
                                  fontSize: 18,
                                  color: MyColors.merah),
                            ),
                            SizedBox(height: 15),
                          ],
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              'ABOUT US',
                              style: TextStyle(
                                  fontFamily: MyFonts.dmsans,
                                  fontSize: 18,
                                  fontWeight: FontWeight.w500,
                                  color: MyColors.merah),
                            ),
                            SizedBox(height: 15),
                            Text(
                              'News',
                              style: TextStyle(
                                  fontFamily: MyFonts.dmsans,
                                  fontSize: 18,
                                  color: MyColors.merah),
                            ),
                            SizedBox(height: 15),
                            Text(
                              'Careers',
                              style: TextStyle(
                                  fontFamily: MyFonts.dmsans,
                                  fontSize: 18,
                                  color: MyColors.merah),
                            ),
                            SizedBox(height: 15),
                            Text(
                              'Investor',
                              style: TextStyle(
                                  fontFamily: MyFonts.dmsans,
                                  fontSize: 18,
                                  color: MyColors.merah),
                            ),
                            SizedBox(height: 15),
                            Text(
                              'Sustainability',
                              style: TextStyle(
                                  fontFamily: MyFonts.dmsans,
                                  fontSize: 18,
                                  color: MyColors.merah),
                            ),
                            SizedBox(height: 15),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
                SizedBox(
                  height: 95,
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
