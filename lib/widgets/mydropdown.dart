// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables, prefer_typing_uninitialized_variables

import 'package:atlan_sport/services/mycolors.dart';
import 'package:flutter/material.dart';

import 'mytext.dart';

class MyDropdown extends StatelessWidget {
  const MyDropdown({
    Key? key,
    TextEditingController? this.controller,
    bool? this.showTitle = true,
    String this.label = "",
    double this.height = 48,
    Color? this.labelColor = MyColors.darkGrey,
    FontWeight? this.labelWeight = FontWeight.w600,
    String? this.value,
    required List<DropdownMenuItem<Object>>? this.items,
    required ValueChanged? this.onChanged,
  }) : super(key: key);

  final controller,
      height,
      showTitle,
      label,
      labelColor,
      labelWeight,
      value,
      items,
      onChanged;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Visibility(
          visible: showTitle,
          child: Column(
            children: [
              MyText(text: label),
              SizedBox(height: 10),
            ],
          ),
        ),
        Container(
          padding: EdgeInsets.symmetric(horizontal: 20),
          height: height,
          decoration: BoxDecoration(
            color: MyColors.whiteColor,
            borderRadius: BorderRadius.circular(5),
            border: Border.all(color: MyColors.darkGrey),
          ),
          child: DropdownButtonHideUnderline(
            child: DropdownButton(
              hint: MyText(
                text: value,
                fontSize: 20,
              ),
              // value: value,
              items: items,
              isExpanded: true,
              icon: Icon(
                Icons.keyboard_arrow_down,
                color: MyColors.darkGrey,
                size: 22,
              ),
              onChanged: onChanged,
            ),
          ),
        )
      ],
    );
  }
}
