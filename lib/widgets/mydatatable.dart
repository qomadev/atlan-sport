import 'package:atlan_sport/services/mycolors.dart';
import 'package:atlan_sport/widgets/mytext.dart';
import 'package:flutter/material.dart';

class MyDataTable extends StatelessWidget {
  const MyDataTable({
    Key? key,
    required String this.text,
  }) : super(key: key);

  final text;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20),
      height: 45,
      color: MyColors.abuIsiTabel,
      child: Center(
          child: MyText(text: text, fontSize: 18, fontColor: MyColors.hitam)),
    );
  }
}
