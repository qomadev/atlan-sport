import 'package:atlan_sport/services/mycolors.dart';
import 'package:atlan_sport/views/internal/inventorystock_page.dart';
import 'package:atlan_sport/views/internal/laporan/pengeluaran.dart';
import 'package:atlan_sport/views/internal/laporan/penjualan.dart';
import 'package:atlan_sport/views/internal/penjualan/direct_order.dart';
import 'package:atlan_sport/views/internal/penjualan/mutasi.dart';
import 'package:atlan_sport/views/internal/penjualan/purchase_order.dart';
import 'package:atlan_sport/views/internal/register_barang/stock_indirect.dart';
import 'package:atlan_sport/views/internal/register_barang/stock_sales.dart';
import 'package:atlan_sport/widgets/mytext.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class MyDrawer extends StatefulWidget {
  const MyDrawer({Key? key}) : super(key: key);

  @override
  _MyDrawerState createState() => _MyDrawerState();
}

class _MyDrawerState extends State<MyDrawer> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 336,
      color: MyColors.whiteColor,
      child: Column(
        children: [
          SizedBox(height: 60),
          DrawerPenjualan(),
          SizedBox(height: 20),
          RegisterBarang(),
          SizedBox(height: 20),
          InventoryStock(),
          SizedBox(height: 20),
          Report(),
          SizedBox(height: 20),
          ProductionControl()
        ],
      ),
    );
  }
}

class ProductionControl extends StatelessWidget {
  const ProductionControl({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: 50),
      child: MyText(
        text: "Production Control",
        fontColor: MyColors.biru,
        fontWeight: FontWeight.w700,
      ),
    );
  }
}

class Report extends StatelessWidget {
  const Report({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        MyText(
            text: "Report",
            fontWeight: FontWeight.w700,
            fontColor: MyColors.hitam),
        SizedBox(height: 10),
        GestureDetector(
          onTap: () => Get.to(Penjualan()),
          child: Padding(
            padding: const EdgeInsets.only(left: 30),
            child: MyText(
              text: "Penjualan",
              fontColor: MyColors.biru,
              fontSize: 20,
            ),
          ),
        ),
        SizedBox(height: 4),
        GestureDetector(
          onTap: () => Get.to(Pengeluaran()),
          child: Padding(
            padding: const EdgeInsets.only(left: 30),
            child: MyText(
              text: "Kas Pengeluaran",
              fontColor: MyColors.biru,
              fontSize: 20,
            ),
          ),
        ),
      ],
    );
  }
}

class InventoryStock extends StatelessWidget {
  const InventoryStock({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => Get.to(InventoryStockPage()),
      child: MyText(
        text: "Inventory Stock",
        fontColor: MyColors.biru,
        fontWeight: FontWeight.w700,
      ),
    );
  }
}

class RegisterBarang extends StatelessWidget {
  const RegisterBarang({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        MyText(
            text: "Register Barang",
            fontWeight: FontWeight.w700,
            fontColor: MyColors.hitam),
        SizedBox(height: 10),
        GestureDetector(
          onTap: () => Get.to(StockSales()),
          child: Padding(
            padding: const EdgeInsets.only(left: 30),
            child: MyText(
              text: "Stock Sales",
              fontColor: MyColors.biru,
              fontSize: 20,
            ),
          ),
        ),
        SizedBox(height: 4),
        GestureDetector(
          onTap: () => Get.to(StockIndirect()),
          child: Padding(
            padding: const EdgeInsets.only(left: 30),
            child: MyText(
              text: "Stock Indirect",
              fontColor: MyColors.biru,
              fontSize: 20,
            ),
          ),
        ),
      ],
    );
  }
}

class DrawerPenjualan extends StatelessWidget {
  const DrawerPenjualan({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        MyText(
            text: "Penjualan",
            fontWeight: FontWeight.w700,
            fontColor: MyColors.hitam),
        SizedBox(height: 10),
        GestureDetector(
          onTap: () => Get.to(DirectOrder()),
          child: Padding(
            padding: const EdgeInsets.only(left: 30),
            child: MyText(
              text: "Direct Order",
              fontColor: MyColors.biru,
              fontSize: 20,
            ),
          ),
        ),
        SizedBox(height: 4),
        GestureDetector(
          onTap: () => Get.to(PurchaseOrder()),
          child: Padding(
            padding: const EdgeInsets.only(left: 30),
            child: MyText(
              text: "Purchase Order",
              fontColor: MyColors.biru,
              fontSize: 20,
            ),
          ),
        ),
        SizedBox(height: 4),
        GestureDetector(
          onTap: () => Get.to(Mutasi()),
          child: Padding(
            padding: const EdgeInsets.only(left: 30),
            child: MyText(
              text: "Mutasi",
              fontColor: MyColors.biru,
              fontSize: 20,
            ),
          ),
        ),
      ],
    );
  }
}
