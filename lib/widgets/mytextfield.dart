import 'package:flutter/material.dart';
import 'package:atlan_sport/services/mycolors.dart';
import 'package:atlan_sport/services/myfonts.dart';
import 'package:intl/intl.dart';

import 'mytext.dart';

class MyTextField extends StatelessWidget {
  const MyTextField({
    Key? key,
    double this.height = 48,
    String this.label = "",
    TextEditingController? this.controller,
    String? this.hintText,
    bool this.showLabel = true,
  }) : super(key: key);

  final controller, height, label, hintText, showLabel;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Visibility(
          visible: showLabel,
          child: Column(
            children: [
              MyText(text: label),
              SizedBox(height: 10),
            ],
          ),
        ),
        Container(
          padding: EdgeInsets.symmetric(horizontal: 20),
          height: height,
          decoration: BoxDecoration(
            color: MyColors.whiteColor,
            borderRadius: BorderRadius.circular(5),
            border: Border.all(color: MyColors.darkGrey),
          ),
          child: TextField(
            controller: controller,
            cursorColor: MyColors.biru,
            style: TextStyle(
              color: MyColors.darkGrey,
              fontFamily: MyFonts.dmsans,
              fontSize: 20,
            ),
            decoration: InputDecoration(
              hintText: hintText,
              border: InputBorder.none,
              hintStyle: TextStyle(
                color: MyColors.darkGrey,
                fontFamily: MyFonts.dmsans,
                fontSize: 20,
              ),
            ),
          ),
        ),
      ],
    );
  }
}

class MyTextFieldNavy extends StatelessWidget {
  const MyTextFieldNavy({
    Key? key,
    TextEditingController? this.controller,
    required String this.hintText,
    bool this.obscure = false,
  }) : super(key: key);

  final controller, hintText, obscure;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20),
      decoration: BoxDecoration(
          color: MyColors.whiteColor,
          borderRadius: BorderRadius.circular(10),
          border: Border.all(color: MyColors.biru)),
      child: TextField(
        controller: controller,
        cursorColor: MyColors.biru,
        obscureText: obscure,
        style: TextStyle(
          color: MyColors.biru,
          fontFamily: MyFonts.dmsans,
          fontSize: 16,
        ),
        decoration: InputDecoration(
          hintText: hintText,
          border: InputBorder.none,
          hintStyle: TextStyle(
            color: MyColors.biru,
            fontFamily: MyFonts.dmsans,
            fontSize: 16,
          ),
        ),
      ),
    );
  }
}

class MyTextFieldAbu extends StatelessWidget {
  const MyTextFieldAbu({
    Key? key,
    double this.height = 40,
    TextEditingController? this.controller,
    String? this.hintText,
    bool this.obscure = false,
  }) : super(key: key);

  final controller, hintText, obscure, height;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20),
      decoration: BoxDecoration(
        color: MyColors.bgAbu,
        borderRadius: BorderRadius.circular(5),
      ),
      child: TextField(
        controller: controller,
        cursorColor: MyColors.hitam,
        obscureText: obscure,
        style: TextStyle(
          color: MyColors.hitam,
          fontFamily: MyFonts.dmsans,
          fontSize: 16,
        ),
        decoration: InputDecoration(
          hintText: hintText,
          border: InputBorder.none,
          hintStyle: TextStyle(
            color: MyColors.hitam,
            fontFamily: MyFonts.dmsans,
            fontSize: 16,
          ),
        ),
      ),
    );
  }
}

class MyTextFieldDate extends StatefulWidget {
  const MyTextFieldDate({
    Key? key,
    double this.height = 48,
    String this.label = "",
    bool this.showLabel = true,
  }) : super(key: key);

  final height, label, showLabel;

  @override
  State<MyTextFieldDate> createState() => _MyTextFieldDateState();
}

class _MyTextFieldDateState extends State<MyTextFieldDate> {
  DateTime _dateTime = DateTime.now();

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Visibility(
          visible: widget.showLabel,
          child: Column(
            children: [
              MyText(text: widget.label),
              SizedBox(height: 10),
            ],
          ),
        ),
        Container(
          // padding: EdgeInsets.symmetric(horizontal: 20),
          height: widget.height,
          decoration: BoxDecoration(
            color: MyColors.whiteColor,
            borderRadius: BorderRadius.circular(5),
            border: Border.all(color: MyColors.darkGrey),
          ),
          child: GestureDetector(
            onTap: () async {
              showDatePicker(
                      context: context,
                      initialDate: _dateTime,
                      firstDate: DateTime(2000),
                      lastDate: DateTime(2040))
                  .then((value) {
                if (value != null) {
                  setState(() {
                    _dateTime = value;
                  });
                }
              });
            },
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                MyText(
                  text: DateFormat("dd MMMM yyyy").format(_dateTime),
                  fontSize: 15,
                ),
                Icon(
                  Icons.date_range_outlined,
                  color: MyColors.darkGrey,
                )
              ],
            ),
          ),
        ),
      ],
    );
  }
}

class MyTextFieldSearch extends StatelessWidget {
  const MyTextFieldSearch({
    Key? key,
    double this.height = 40,
    TextEditingController? this.controller,
    String? this.hintText,
    bool this.obscure = false,
  }) : super(key: key);

  final controller, hintText, obscure, height;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20),
      decoration: BoxDecoration(
        color: MyColors.bgAbu,
        borderRadius: BorderRadius.circular(5),
      ),
      child: TextField(
        controller: controller,
        cursorColor: MyColors.hitam,
        obscureText: obscure,
        style: TextStyle(
          color: MyColors.hitam,
          fontFamily: MyFonts.dmsans,
          fontSize: 16,
        ),
        decoration: InputDecoration(
          hintText: hintText,
          border: InputBorder.none,
          hintStyle: TextStyle(
            color: MyColors.hitam,
            fontFamily: MyFonts.dmsans,
            fontSize: 16,
          ),
          suffixIcon: Icon(Icons.search_sharp, color: MyColors.darkGrey,)
        ),
      ),
    );
  }
}
