import 'package:flutter/material.dart';

class Header extends StatelessWidget {
  const Header({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      margin: EdgeInsets.only(top: 54, bottom: 100),
      child: Image.asset(
        'assets/images/login/logo_atlan.png',
        width: 230,
        height: 52,
      ),
    );
  }
}
