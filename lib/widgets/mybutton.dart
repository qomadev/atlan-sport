import 'package:atlan_sport/services/mycolors.dart';
import 'package:atlan_sport/services/myfonts.dart';
import 'package:flutter/material.dart';

class MyButton extends StatelessWidget {
  const MyButton(
      {Key? key,
      required VoidCallback? this.onPressed,
      required String this.text,
      Widget? this.icon})
      : super(key: key);

  final onPressed, text, icon;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 30,
      width: 80,
      child: ElevatedButton(
        onPressed: onPressed,
        style: ButtonStyle(
          backgroundColor: MaterialStateProperty.all(MyColors.merah),
          shape: MaterialStateProperty.all(
            RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(100),
            ),
          ),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            (icon == null) ? const SizedBox() : SizedBox(),
            Center(
              child: Text(text,
                  textAlign: TextAlign.center,
                  style: const TextStyle(
                      color: MyColors.whiteColor,
                      fontFamily: MyFonts.dmsans,
                      fontSize: 10,
                      fontWeight: FontWeight.w700,
                      letterSpacing: 1)),
            )
          ],
        ),
      ),
    );
  }
}
