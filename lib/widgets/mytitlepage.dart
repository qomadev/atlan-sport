import 'package:atlan_sport/services/mycolors.dart';
import 'package:atlan_sport/services/myfonts.dart';
import 'package:flutter/material.dart';

class MyTitlepage extends StatelessWidget {
  const MyTitlepage({
    Key? key,
    required String this.text,
  }) : super(key: key);

  final text;

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      style: TextStyle(
          fontFamily: MyFonts.dmsans,
          fontSize: 24,
          fontWeight: FontWeight.w700,
          color: MyColors.darkGrey),
    );
  }
}
