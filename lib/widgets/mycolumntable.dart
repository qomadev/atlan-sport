import 'package:atlan_sport/services/mycolors.dart';
import 'package:atlan_sport/widgets/mytext.dart';
import 'package:flutter/material.dart';

class MyColumnTable extends StatelessWidget {
  const MyColumnTable({
    Key? key,
    required String this.label,
  }) : super(key: key);

  final label;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20),
      height: 45,
      color: MyColors.biruJudulTable,
      child: Center(
          child: MyText(text: label, fontSize: 18, fontColor: MyColors.hitam)),
    );
  }
}
