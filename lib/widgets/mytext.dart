import 'package:atlan_sport/services/mycolors.dart';
import 'package:atlan_sport/services/myfonts.dart';
import 'package:flutter/material.dart';

class MyText extends StatelessWidget {
  const MyText({
    Key? key,
    required String this.text,
    double this.fontSize = 24,
    Color this.fontColor = MyColors.darkGrey,
    FontWeight this.fontWeight = FontWeight.w400,
    String this.fontFamily = MyFonts.dmsans,
  }) : super(key: key);

  final text, fontSize, fontColor, fontWeight, fontFamily;

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      style: TextStyle(
        fontFamily: fontFamily,
        fontSize: fontSize,
        fontWeight: fontWeight,
        color: fontColor,
      ),
    );
  }
}
