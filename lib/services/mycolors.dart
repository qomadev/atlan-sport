import 'package:flutter/material.dart';

class MyColors {
  static const Color primaryColor = Color.fromRGBO(200, 200, 200, 1);
  static const Color merah = Color.fromRGBO(194, 39, 21, 1);
  static const Color biru = Color.fromRGBO(43, 71, 107, 1);
  static const Color abuIsiTabel = Color.fromRGBO(220, 219, 223, 1);
  static const Color grey1Color = Color.fromRGBO(20, 20, 20, 120);
  static const Color darkGrey = Color.fromRGBO(69, 66, 66, 1);
  static const Color whiteColor = Color.fromRGBO(255, 255, 255, 1);
  static const Color hitam = Color.fromRGBO(0, 0, 0, 1);
  static const Color biruJudulTable = Color.fromRGBO(167, 188, 214, 1);
  static const Color bgTable = Color.fromRGBO(252, 250, 250, 1);
  static const Color biruGaris = Color.fromRGBO(61, 169, 243, 1);
  static const Color bgAbu = Color.fromRGBO(220, 219, 223, 1);
}
